let miMapa = L.map('mapid');


miMapa.setView([4.598684,-74.098270], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(miMapa);


let miMarcador = L.marker([4.598684,-74.098270]);
miMarcador.addTo(miMapa);
miMarcador.bindPopup(
      "Planta: Salvia.</br><br>Agricultor: Caterina Masciullo.</br><br>Proyecto: Plantas comunitarias.</br><br> Coordenadas: 4.598684,-74.098270 </br><br>"
    );
let sitio = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              4.598879296242767,
              -74.09831866621971
            ],
            [
              4.598689472498917,
              -74.09848764538765
            ],
            [
              4.598530394815467,
              -74.09830927848816
            ],
            [
              4.5987429439811685,
              -74.0981362760067
            ],
            [
              4.598879296242767,
              -74.09831866621971
            ]
          ]
        ]
      }
    }
  ]
};
let miGeoJSON = L.GeoJSON(sitio);
miGeoJSON.addTo(miMapa);
polygon.addto(miMapa);

 







